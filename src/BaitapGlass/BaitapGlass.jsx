import React, { Component } from "react";
import { glassArr } from "./DataGlass";

export default class BaitapGlass extends Component {
  state = {
    glassDetail: glassArr[0],
    imgSrc: "./glasses/glassesImage/v1.png",
  };

  handleChangeImg = (stt) => {
    let newImg = `./glasses/glassesImage/v${stt}.png`;
    this.setState({
      imgSrc: newImg,
    });
  };

  renderGlassDetail = () => {
    let glassList = glassArr.map((item) => {
      return (
        <div className="card col-2">
          <img
            className="card-img-top"
            src={`./glasses/glassesImage/v${item.id}.png`}
            alt="Card image cap"
          />
          <div className="card-body">
            <h4 className="card-title">{item.name}</h4>
            <p className="card-text">{item.price} $</p>
            <p className="card-text">Type: {item.id}</p>
          </div>
          <button
            onClick={() => {
              this.handleChangeImg(item.id);
            }}
            className="btn btn-warning"
          >
            Change
          </button>
        </div>
      );
    });
    return glassList;
  };
  render() {
    return (
      <div className="container" style={{ margin: "0 auto" }}>
        <div style={{position:"relative"}}>
          <img src="./glasses/glassesImage/model.jpg" alt="" />
          <img
            src={this.state.imgSrc}
            style={{ position: "absolute", width:"250px", left:"430px", top:"150px" }}
            alt=""
          />
        </div>

        <div className="row">{this.renderGlassDetail()}</div>
      </div>
    );
  }
}
